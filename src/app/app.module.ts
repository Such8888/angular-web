import { MissionVisionComponent } from './about-us/mission-vision/mission-vision.component';
import { IntroductionComponent } from './about-us/introduction/introduction.component';
import { HeaderComponent } from './layouts/header/header.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {ReactiveFormsModule, FormsModule} from '@angular/forms'
import {OrderModule} from 'ngx-order-pipe';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ServicesComponent } from './services/services.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { CategoriesComponent } from './categories/categories.component';
import { BlogComponent } from './blog/blog.component';
import { ContactComponent } from './contact/contact.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { RegisterComponent } from './register/register.component';
import { SinglePageComponent } from './single-page/single-page.component';
import { GalleryComponent } from './gallery/gallery.component';
import { AlbumListComponent } from './gallery/album-list/album-list.component';
import { MainBannerComponent } from './gallery/main-banner/main-banner.component';
import { PhotosComponent } from './gallery/photos/photos.component';
import { NewsComponent } from './news/news.component';
import { CompanyProfileComponent } from './about-us/company-profile/company-profile.component';
import { ContactListComponent } from './contact/contact-list.component';
import { GridModule } from '@progress/kendo-angular-grid';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';







@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ServicesComponent,
    AboutUsComponent,
    CategoriesComponent,
    BlogComponent,
    ContactComponent,
    HeaderComponent,
    FooterComponent,
    SignInComponent,
    RegisterComponent,
    SinglePageComponent,
    GalleryComponent,
    AlbumListComponent,
    MainBannerComponent,
    PhotosComponent,
    NewsComponent,
    IntroductionComponent,
    MissionVisionComponent,
    CompanyProfileComponent,
    ContactListComponent
   
  
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    OrderModule,
    GridModule,
    BrowserAnimationsModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
