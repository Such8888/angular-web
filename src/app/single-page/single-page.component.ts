import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-single-page',
  templateUrl: './single-page.component.html',
  styleUrls: ['./single-page.component.css']
})
export class SinglePageComponent implements OnInit {
formData;
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.formData=this.fb.group({
      name: '',
      email:'',
      phone:'',
      subject: '',
      comments: ''
    })
  }

}
