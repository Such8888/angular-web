import { ContactListComponent } from './contact/contact-list.component';
import { CompanyProfileComponent } from './about-us/company-profile/company-profile.component';
import { MissionVisionComponent } from './about-us/mission-vision/mission-vision.component';
import { IntroductionComponent } from './about-us/introduction/introduction.component';
import { NewsComponent } from './news/news.component';
import { GalleryComponent } from './gallery/gallery.component';
import { SinglePageComponent } from './single-page/single-page.component';
import { RegisterComponent } from './register/register.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { ContactComponent } from './contact/contact.component';
import { CategoriesComponent } from './categories/categories.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServicesComponent } from './services/services.component';
import { BlogComponent } from './blog/blog.component';


const routes: Routes = [
{path: 'home', component: HomeComponent},
{path: 'about', component: AboutUsComponent},
{path: 'service', component: ServicesComponent},
{path: 'categories', component: CategoriesComponent},
{path: 'blog', component: BlogComponent},
{path: 'contact', component: ContactComponent},
{path: 'signin', component: SignInComponent},
{path: 'contact', component: ContactComponent},
{path:'news', component: NewsComponent},
{path: 'sign', component: SignInComponent},
{path: 'register', component: RegisterComponent},
{path: 'post/introduction', component: IntroductionComponent},
{path: 'post/mision-vision', component: MissionVisionComponent},
{path: 'post/company-profile', component: CompanyProfileComponent}, 
{path: 'single', component: SinglePageComponent}, 
{path: 'gallery', component: GalleryComponent},
{path: 'contactlist', component: ContactListComponent},
{path:'',redirectTo:'/home',pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
