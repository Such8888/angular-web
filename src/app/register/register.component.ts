import { Router } from '@angular/router';
import { ApiService } from './../services/api.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
formData;
  constructor(private fb: FormBuilder,
                          private Api: ApiService,
                          private router: Router) { }

  ngOnInit() {
this.formData = this.fb.group({
 name: '',
 email: '',
 phone: '',
 password: '' 
})
  }

onSubmit(){
  this.Api.addRegister(this.formData.value).subscribe(() => {
    this.router.navigate (['/register'])
    this.formData.reset()
  });
  console.log(this.formData.value);
}

}
