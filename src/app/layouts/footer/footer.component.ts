import { Router } from '@angular/router';
import { LayoutService } from './../services/layout.service';
import { Component, OnInit, Input, AfterViewChecked, OnChanges } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  imagePath:  "https://bentray.work/bentray-cms/public/images/";
  @Input() data;
  settings: any;

  constructor(private router: Router  ) { }

  ngOnInit() {
      this.settings = this.data;
      console.log(this.data);
  }

  onClick(){
this.router.navigate(['/about']);
  }



  // get the data from the service variable
  // use  interpolation to display the data
}
