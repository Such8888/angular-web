import { LayoutService } from './../services/layout.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  menuItem;
  subMenuAbout;
  constructor(
    private service: LayoutService
  ) { }

  ngOnInit() {
    this.service.getMenu().subscribe((data) => {
      const dataArray: any = data;
      this.menuItem=dataArray.filter((item) =>   item.menu_parent ==0);
      this.subMenuAbout = dataArray.filter((item) => item.menu_parent==19);
    })
  }
}