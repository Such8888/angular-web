import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LayoutService {
  private url = environment.url;
  constructor(
      private http:HttpClient
    ) { }

    getSettingsData(){
    return this.http.get(this.url + 'settings');
    }



  settings: any;

  //getSetting()

  setSettings(value: any) {
    this.settings = value;
  }

  getSettings() {
    return this.settings;
  }

  getMenu(){
    return this.http.get(this.url + 'menuitems')
  }
}
