import { LayoutService } from './layouts/services/layout.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Furnish';
  settings;
  constructor(
    private layoutService: LayoutService
  ) { }

  ngOnInit() {
    this.layoutService.getSettingsData().subscribe(data=>{
      if(data[0]) {
        this.settings = data[0];
        console.log(this.settings);
      }
    })
  }
  // call settings API from  layoutService
  //and store the response in a settings variable in the  layout service using setSetting();
}
