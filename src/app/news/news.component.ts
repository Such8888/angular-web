import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
news

imagePath = "https://bentray.work/bentray-cms/public/images/";
  constructor(private service: ApiService) { }

  ngOnInit() {
    this.service.getNews().subscribe(data=> {
    this.news = data;

    console.log(this.news);
    })
  }

}
