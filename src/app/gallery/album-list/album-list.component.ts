import { ApiService } from './../../services/api.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-album-list',
  templateUrl: './album-list.component.html',
  styleUrls: ['./album-list.component.css']
})
export class AlbumListComponent implements OnInit {
albums: Object
  constructor(private Api: ApiService) { }

  ngOnInit() {
this.Api.getAlbum().subscribe((data) => {
this.albums = data;
console.log(this.albums);
})
  }

}
