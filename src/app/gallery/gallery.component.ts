import { ApiService } from './../services/api.service';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {
imagePath = "https://bentray.work/bentray-cms/public/images/";

galleries;
  constructor(private Api: ApiService ) { }

  ngOnInit() {
this.Api.getGallery().subscribe((data) => {
this.galleries = data;
console.log(this.galleries);
})
  }


}
