import { ApiService } from './../services/api.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  public categoryData: Object;
  constructor(private apiS: ApiService) { }

  ngOnInit() {
    this.apiS.getCategory().subscribe((data) => {
      this.categoryData = data;
      console.log(this.categoryData);
    })
  }

}
