import { ApiService } from './../services/api.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
formData;
  constructor(private fb: FormBuilder,
                          private service: ApiService,
                          private router: Router) { }

  ngOnInit() {
this.formData = this.fb.group({
name: '',
phone: '',
email: '',
address: '',
comments: ''  
})   
  }

onSubmit(){
this.service.addContact(this.formData.value).subscribe((data) => {
this.router.navigate(['/contact']) 
this.formData.reset()
});
console.log(this.formData.value);
}

}
