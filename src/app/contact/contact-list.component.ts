import { ApiService } from "./../services/api.service";
import { Component, OnInit } from "@angular/core";
import {
  CompositeFilterDescriptor,
  filterBy,
  State,
  SortDescriptor
} from "@progress/kendo-data-query";
import { PageChangeEvent, GridDataResult } from "@progress/kendo-angular-grid";
import { state } from "@angular/animations";

@Component({
  selector: "app-contact-list",
  templateUrl: "./contact-list.component.html",
  styleUrls: ["./contact-list.component.css"]
})
export class ContactListComponent implements OnInit {
  constructor(private service: ApiService) {}
  contactList: any = [];
  public pageSize: number = 10;
  public pageNumber: number;
  public skip = 0;
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [
    {
      field: "",
      dir: "asc"
    }
  ];

  nameSearchKey: string;
  emailSearchKey: string;
  phoneSearchKey: string;
  addressSearchKey: string;
  gridView: GridDataResult;
  ngOnInit() {
    this.getContactLists();
  }

  getContactLists(): void {
    const obj = {
      limit: this.pageSize,
      page: this.pageNumber,
      sortno: 1,
      sortnane: "",
      search: {
        name: this.nameSearchKey ? this.nameSearchKey : "",
        email: this.emailSearchKey ? this.emailSearchKey : "",
        phone: this.phoneSearchKey ? this.phoneSearchKey : "",
        address: this.addressSearchKey ? this.addressSearchKey : ""
      }
    };
    this.service.contactsList(obj).subscribe((res: any) => {
      this.contactList = res.data;
      this.gridView = {
        data: this.contactList,
        total: res.count ? res.count : 0

        // total: res ["count"] || 0
      };
    });
  }

  pageChange({ skip, take }: PageChangeEvent): void {
    this.skip = skip;
    this.pageNumber = skip / take + 1;
    this.getContactLists();
  }

  public filterChange(filter): void {
    this.nameSearchKey = "";
    this.emailSearchKey = "";
    this.phoneSearchKey = "";
    this.addressSearchKey = "";
    this.filter = filter;
    const filterData = filter.filters;
    console.log("SDSD" + JSON.stringify(filterData));
    // {"filters":[{"field":"contact_id","operator":"contains","value":"24"},
    // {"field":"name","operator":"contains","value":"dsad"},
    // {"field":"email","operator":"contains","value":"dcsad"}],"logic":"and"}
    // console.log("ddsadas" + JSON.stringify(this.filter));
    for (let i = 0; i < filterData.length; i++) {
      if (filterData[i].field == "name") {
        this.nameSearchKey = filterData[i].value;
      }

      if (filterData[i].field == "email") {
        this.emailSearchKey = filter.filters[i].value;
      }
      if (filterData[i].field == "phone") {
        this.phoneSearchKey = filter.filters[i].value;
      }

      if (filterData[i].field == "address") {
        this.addressSearchKey = filter.filters[i].value;
      }
    }

    this.getContactLists();
  }

  sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.getContactLists();
    console.log(sort);
  }
}
