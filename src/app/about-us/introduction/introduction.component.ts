import { ApiService } from "./../../services/api.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-introduction",
  templateUrl: "./introduction.component.html",
  styleUrls: ["./introduction.component.css"]
})
export class IntroductionComponent implements OnInit {
  data: number = 0;
  constructor(private apiService: ApiService) {}

  ngOnInit() {
    this.apiService.getPosts().subscribe((data: any) => {
      this.data = data.find(i => i.post_title === "Introduction");
    });
  }
}
