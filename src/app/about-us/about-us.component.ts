import { LayoutService } from './../layouts/services/layout.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {
about: Object;
  constructor(private aboutService: LayoutService) { }

  ngOnInit() {
this.aboutService.getMenu().subscribe((data) => {
this.about = data;
console.log(this.about);
})
  }

}
