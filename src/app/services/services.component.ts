import { ApiService } from './api.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {
public ApiS: Object
  constructor(private Api: ApiService ) { }

  ngOnInit() {
    this.Api.getServices().subscribe(data => {
  this.ApiS = data;
  console.log(this.ApiS);
    })
  }

}


