import { environment } from "./../../environments/environment";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class ApiService {
  private url = environment.url;
  constructor(private http: HttpClient) {}

  getCategory() {
    return this.http.get(this.url + "categories");
  }
  getServices() {
    return this.http.get(this.url + "services/service");
  }

  getSetting() {
    return this.http.get(this.url + "v1/settings");
  }

  getAlbum() {
    return this.http.get(this.url + "albums");
  }

  getGallery() {
    return this.http.get(this.url + "galleries");
  }

  getMenu() {
    return this.http.get(this.url + "menuitems");
  }

  getNotice() {
    return this.http.get(this.url + "notices");
  }

  getPosts() {
    return this.http.get(this.url + "posts");
  }

  getProducts() {
    return this.http.get(this.url + "products");
  }

  getContact() {
    return this.http.get(this.url + "contacts/add");
  }

  addContact(value: any) {
    return this.http.post(this.url + "contacts/add", value);
  }

  getRegister() {
    return this.http.get(this.url + "contacts/add");
  }

  addRegister(value) {
    return this.http.post(this.url + "contacts/add", value);
  }

  getNews() {
    return this.http.get(this.url + "notices");
  }

  getListContacts() {
    return this.http.get(this.url + "contacts");
  }

  contactsList(body) {
    return this.http.post(`${this.url}contacts/search`, body);
  }
}
